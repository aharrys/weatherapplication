//
//  RegisterViewController.swift
//  WeatherApplication
//
//  Created by Akhmad Harry Susanto on 18/06/20.
//  Copyright © 2020 Akhmad Harry Susanto. All rights reserved.
//

import Foundation
import UIKit

class RegisterViewController: UIViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        navigationController?.topViewController?.title = "Register"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = true
    }

}
